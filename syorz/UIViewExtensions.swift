//
//  UIViewExtensions.swift
//  syorz
//
//  Created by paul calver on 12/03/2016.
//  Copyright © 2016 Qalvapps. All rights reserved.
//

import UIKit

extension UIView {
    
    public func snapshot() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let snapshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return snapshot
    }
}
