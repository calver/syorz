//
//  Constants.swift
//  syorz
//
//  Created by paul calver on 12/03/2016.
//  Copyright © 2016 Qalvapps. All rights reserved.
//

import UIKit
import Foundation

struct AppInfo {
    static let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    static let build = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
    static let systemVersion = UIDevice.current.systemVersion
    static let deviceModelName = UIDevice.current.modelName
    static let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
    static let deviceScreenSize = UIScreen.main.bounds.size
}

struct ImageSettings {
    static let maxHeight: CGFloat = 600.0
    static let maxWidth: CGFloat = 800.0
    static let compressionQuality: CGFloat = 0.5
}

struct TextSettings {
    static let textColorLight = UIColor.white
    static let textColorDark = UIColor.black
    static let textFont = UIFont(name: CustomFonts.fontBoldCondensed, size: 100)
    static let textFontAttributesLight = [
        NSFontAttributeName: TextSettings.textFont!,
        NSForegroundColorAttributeName: TextSettings.textColorLight,
        NSStrokeColorAttributeName: TextSettings.textColorDark,
        NSStrokeWidthAttributeName: -2.0
    ] as [String : Any]
    static let textFontAttributesDark = [
        NSFontAttributeName: TextSettings.textFont!,
        NSForegroundColorAttributeName: TextSettings.textColorDark,
        NSStrokeColorAttributeName: TextSettings.textColorLight,
        NSStrokeWidthAttributeName: -2.0
    ] as [String : Any]
}

struct KeyStrings {
    static let syorz = "YOURS"
}

struct CustomFonts {
    static let fontLightCondensed = "AkzidenzGroteskBQ-LigCnd"
    static let fontMedium = "AkzidenzGroteskBQ-Medium"
    static let fontLight = "AkzidenzGroteskBQ-Light"
    static let fontSuper = "AkzidenzGroteskBQ-Super"
    static let fontRegular = "AkzidenzGroteskBQ-Reg"
    static let fontBoldCondensed = "AkzidenzGroteskBQ-BdCnd"
}
