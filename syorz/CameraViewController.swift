//
//  CameraViewController.swift
//  syorz
//
//  Created by paul calver on 12/03/2016.
//  Copyright © 2016 Qalvapps. All rights reserved.
//

import UIKit
import AVFoundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CameraViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var cameraContainer: UIView!
    @IBOutlet weak var textField: UITextField!
    
    //cropping state
    @IBOutlet weak var darkLightButton: UIButton!
    @IBOutlet weak var scrollView: ImageScrollView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var navBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textLabelBottomConstraint: NSLayoutConstraint!
    
    let picker = UIImagePickerController()
    
    var imageToUse: UIImage?
    var imageWithText: UIImage?
    
    let screenSize = UIScreen.main.bounds
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    var stillImageOutput = AVCaptureStillImageOutput()
    var imageData: Data!
    var capturedImage: UIImage?
    var textIsTransitioning = false
    var textIsLight = true {
        didSet {
            updateTextStyle()
        }
    }
    
    var inCameraMode = true {
        didSet {
            triggerTransition()
        }
    }
    
    var canTakePhoto = true
    var canPresentImagePicker = true
    
    var originalTextConstraintConstant: CGFloat = 0
    
    // UI Buttons - camera state
    var flipCameraButton: UIButton?
    var lightDarkTextButton: UIButton?
    var takePhotoButton: UIButton?
    var photoLibraryButton: UIButton?
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var captureDevicePosition: AVCaptureDevicePosition?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        addObservers()
        setupCameraSession()
        createUIElements()
        styleUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupCameraSession() {
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        assignCameraDevice(AVCaptureDevicePosition.back)
        
        if captureDevice != nil {
            DLog("device found")
            beginSession()
        }
    }
    func addObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CameraViewController.keyboardShown(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(CameraViewController.keyboardHidden(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    func assignCameraDevice(_ capturePosition:AVCaptureDevicePosition) {
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices! {
            // Make sure this particular device supports video
            if ((device as AnyObject).hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if((device as AnyObject).position == capturePosition) {
                    captureDevice = device as? AVCaptureDevice
                    captureDevicePosition = capturePosition
                }
            }
        }
    }
    
    func toggleCamera () {
        
        captureSession.beginConfiguration()
        
        // remove current input
        
        if let currentCameraInput = captureSession.inputs[0] as? AVCaptureInput {
            captureSession.removeInput(currentCameraInput)
        }
        
        // get new input
        
        if (captureDevicePosition == AVCaptureDevicePosition.back) {
            assignCameraDevice(AVCaptureDevicePosition.front)
        } else {
            assignCameraDevice(AVCaptureDevicePosition.back)
        }
        
        configureDevice()
        
        let err : NSError? = nil
        
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
        } catch {
            // handle error
            return
        }
        
        if err != nil {
            DLog("error: \(err?.localizedDescription)")
        }
        
        captureSession.commitConfiguration()
    }
    
    func updateDeviceSettings(_ focusValue : Float, isoValue : Float) {
        if let device = captureDevice {
            
            do {
                try device.lockForConfiguration()
            } catch {
                // handle error
                return
            }
            
            
            device.setFocusModeLockedWithLensPosition(focusValue, completionHandler: { (time) -> Void in
                //
            })
            
            // Adjust the iso to clamp between minIso and maxIso based on the active format
            let minISO = device.activeFormat.minISO
            let maxISO = device.activeFormat.maxISO
            let clampedISO = isoValue * (maxISO - minISO) + minISO
            
            device.setExposureModeCustomWithDuration(AVCaptureExposureDurationCurrent, iso: clampedISO, completionHandler: { (time) -> Void in
                //
            })
            
            device.unlockForConfiguration()
        }
    }
    
    func configureDevice() {
        
        if let device = captureDevice {
            
            do {
                try device.lockForConfiguration()
            } catch {
                // handle error
                return
            }
            
            if (device.isFocusModeSupported(.continuousAutoFocus)) {
                device.focusMode = AVCaptureFocusMode.continuousAutoFocus
            }
            device.unlockForConfiguration()
            
        }
    }
    
    func beginSession() {
        
        configureDevice()
        
        let err : NSError? = nil
        
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
        } catch {
            // handle error
            return
        }
        
        
        if err != nil {
            DLog("error: \(err?.localizedDescription)")
        }
        
        
        captureSession.startRunning()
        
        stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
        if let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) {
            
            previewLayer.frame = view.bounds
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.cameraContainer.layer.addSublayer(previewLayer)
        }
        
        // add text 
        updateTextLabel()
    }
    
    func captureStillImage () {
        
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection)
                { (imageDataSampleBuffer, error) -> Void in
                    self.imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                    
                    self.capturedImage = UIImage(data: self.imageData)
                    
                    DispatchQueue.main.async(execute: {
                        //self.presentCropPhotoVC()
                        //self.transitionToCropAndShare()
                        self.inCameraMode = false
                    })
                    
                    
            }}
        
//        capturedImage = cameraContainer!.snapshot()
//        presentCropPhotoVC()
    }
    
    // MARK UI
    func createUIElements() {
        setupButtons()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CameraViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func styleUI() {
        shareButton.backgroundColor = UIColor.black
        scrollView.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func setupButtons() {
        
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        
        flipCameraButton = UIButton()
        flipCameraButton!.setTitle("flip", for: UIControlState())
        flipCameraButton!.setTitleColor(UIColor.white, for: UIControlState())
        flipCameraButton!.frame = CGRect(x: screenWidth-70, y: 20, width: 50, height: 20)
        flipCameraButton!.addTarget(self, action: #selector(CameraViewController.flipCameraButtonPressed(_:)), for: .touchUpInside)
        
        view.addSubview(flipCameraButton!)
        
        lightDarkTextButton = UIButton()
        lightDarkTextButton!.setTitle("l/d", for: UIControlState())
        lightDarkTextButton!.setTitleColor(UIColor.white, for: UIControlState())
        lightDarkTextButton!.frame = CGRect(x: 20, y: screenHeight-40, width: 50, height: 20)
        lightDarkTextButton!.addTarget(self, action: #selector(CameraViewController.lightDarkTextButtonPressed(_:)), for: .touchUpInside)
        
        view.addSubview(lightDarkTextButton!)
        
        takePhotoButton = UIButton()
        takePhotoButton!.setTitle("take", for: UIControlState())
        takePhotoButton!.setTitleColor(UIColor.white, for: UIControlState())
        takePhotoButton!.frame = CGRect(x: (screenWidth/2)-25, y: screenHeight-40, width: 50, height: 20)
        takePhotoButton!.addTarget(self, action: #selector(CameraViewController.takePhotoButtonPressed(_:)), for: .touchUpInside)
        
        view.addSubview(takePhotoButton!)
        
        photoLibraryButton = UIButton()
        photoLibraryButton!.setTitle("lib", for: UIControlState())
        photoLibraryButton!.setTitleColor(UIColor.white, for: UIControlState())
        photoLibraryButton!.frame = CGRect(x: screenWidth-70, y: screenHeight-40, width: 50, height: 20)
        photoLibraryButton!.addTarget(self, action: #selector(CameraViewController.photoLibraryButtonPressed(_:)), for: .touchUpInside)
        
        view.addSubview(photoLibraryButton!)
        
        view.bringSubview(toFront: shareButton!)
        view.bringSubview(toFront: navView!)
    }
    
    
    //MARK: crop and share
    
    func triggerTransition() {
        if inCameraMode {
            view.endEditing(true) // in case keyboard presented
            canTakePhoto = true
            canPresentImagePicker = true
           transitionToCamera()
        } else {
            transitionToCropAndShare()
        }
    }
    
    func transitionToCamera() {
        
        cameraContainer.isHidden = false
        imageView.isHidden = true
        scrollView.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.navBarTopConstraint.constant = -self.navView!.bounds.size.height
            self.imageViewBottomConstraint.constant = 0
            self.imageViewTopConstraint.constant = 0
            self.shareButtonBottomConstraint.constant = -self.shareButton.bounds.size.height
            self.textLabelBottomConstraint.constant = 60
            
            self.view.layoutIfNeeded()
            
            }, completion: { finished in
                
        })
    }
    
    func transitionToCropAndShare() {
        
        let transitionImage = capturedImage
        imageView.image = transitionImage
        
        let compressedImage = compressImage(capturedImage!)
        imageToUse = compressedImage
        
        cameraContainer.isHidden = true
        imageView.isHidden = false
        scrollView.isHidden = true
        
        scrollView.displayImage(imageToUse!)
        
        scrollView.scrollRectToVisible(CGRect(x: scrollView.contentSize.width - 1, y: scrollView.contentSize.height - 1, width: 1, height: 1), animated: false)
        
        let yPositionScrollViewBottom = self.scrollView.frame.height + self.navView!.bounds.size.height // navbar height
        let imageViewOffset = AppInfo.deviceScreenSize.height - yPositionScrollViewBottom
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.navBarTopConstraint.constant = 0
            self.imageViewBottomConstraint.constant = imageViewOffset
            self.imageViewTopConstraint.constant = -imageViewOffset
            self.shareButtonBottomConstraint.constant = 0
            self.textLabelBottomConstraint.constant = imageViewOffset
            
            self.view.layoutIfNeeded()
            
            }, completion: { finished in
                
                self.imageView.isHidden = true
                self.scrollView.isHidden = false
                self.imageViewBottomConstraint.constant = 0
        })
        
    }
    
    //MARK: button actions
    
    func flipCameraButtonPressed(_ sender: UIButton!) {
        toggleCamera()
    }
    func lightDarkTextButtonPressed(_ sender: UIButton!) {
        flipTextStyle()
    }
    
    func takePhotoButtonPressed(_ sender: UIButton!) {
        
        if !canTakePhoto {
            return
        }
        
        canTakePhoto = false
        
        captureStillImage()
        
        if UIDevice.current.model == "iPhone Simulator" {
            DispatchQueue.main.async(execute: {
                //self.presentCropPhotoVC()
                //transitionToCropAndShare()
            })
        }
    }
    
    func photoLibraryButtonPressed(_ sender: UIButton!) {
        presentImagePicker()
    }
    
    @IBAction func didTapRetakeButton(_ sender: AnyObject) {
        inCameraMode = true
        //transitionToCamera()
    }
    
    @IBAction func didTapShareButton(_ sender: AnyObject) {
        // submit photo
        shareWithWhatsapp()
    }
    
    @IBAction func didTapDarkLightButton(_ sender: AnyObject) {
        flipTextStyle()
    }
    
    //MARK: compress and resize image
    
    func compressImage(_ image: UIImage) -> UIImage? {
        
        // settings
        let maxHeight = AppInfo.deviceScreenSize.height //ImageSettings.maxHeight
        let maxWidth = AppInfo.deviceScreenSize.width
        let compressionQuality = ImageSettings.compressionQuality
        
        var actualHeight = image.size.height
        var actualWidth = image.size.width
        var imgRatio = actualWidth/actualHeight
        let maxRatio = maxWidth/maxHeight
        
        if ((actualHeight > maxHeight) || (actualWidth > maxWidth)) {
            
            if (imgRatio < maxRatio) {
                
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
                
            } else if (imgRatio > maxRatio) {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!, compressionQuality)
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData!)
    }
    
    func textToImage(_ drawText: NSString, inImage: UIImage) -> UIImage{
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(inImage.size)
        
        //Put the image into a rectangle as large as the original image.
        inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height))
        
        let attributes  = textLabelAttributes()
        
        let textSize = drawText.size(attributes: attributes)
        
        // Creating a point within the space that is as big as the image.
        let rect: CGRect = CGRect(x: (inImage.size.width - textSize.width)/2, y: (inImage.size.height - textSize.height) + 10, width: inImage.size.width, height: inImage.size.height)
        
        //Now Draw the text into an image.
        drawText.draw(in: rect, withAttributes: attributes)
        
        // Create a new image out of the images we have created
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //And pass it back up to the caller.
        return newImage
        
    }
    
    //MARK: share
    func shareWithWhatsapp() {
        
        let scale = 1 / scrollView.zoomScale
        let visibleRect = CGRect(x: scrollView.contentOffset.x * scale, y: scrollView.contentOffset.y*scale, width: scrollView.bounds.size.width*scale, height: scrollView.bounds.size.height*scale)
        
        let ref = imageToUse!.cgImage?.cropping(to: visibleRect)!
        
        let croppedImage:UIImage = UIImage(cgImage: ref!)
        
        imageWithText = textToImage(displayText() as NSString, inImage: croppedImage)
        
        let activityVC = UIActivityViewController(activityItems: [imageWithText!], applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.mail, UIActivityType.postToTwitter, UIActivityType.postToFacebook, UIActivityType.airDrop, UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.navigationController?.present(activityVC, animated: true, completion: nil)
        
    }
    
    //MARK: text label
    func flipTextStyle() {
        if !textIsTransitioning {
            textIsLight = !textIsLight
            let buttonText = (textIsLight) ? "Dark" : "Light"
            darkLightButton?.setTitle(buttonText, for: UIControlState())
        }
    }
    
    func updateTextStyle() {
        
        let yPositionScrollViewBottom = self.scrollView.frame.height + self.navView!.bounds.size.height
        let imageViewOffset = AppInfo.deviceScreenSize.height - yPositionScrollViewBottom
        
        textIsTransitioning = true
        
        if !inCameraMode {
            textField.alpha = 0
            self.textLabelBottomConstraint.constant = imageViewOffset + 20
            view.layoutIfNeeded()
        }
        
        updateTextLabel()
        
        if !inCameraMode {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                
                self.textField.alpha = 1
                self.textLabelBottomConstraint.constant = imageViewOffset
                
                self.view.layoutIfNeeded()
                
                }, completion: { finished in
                    
                    self.textIsTransitioning = false
            })
        } else {
            self.textIsTransitioning = false
        }
    }
    
    func updateTextLabel() {
        
        
        let outlinedText = NSAttributedString(string: displayText(), attributes: textLabelAttributes())
        textField?.attributedText = outlinedText
        textField?.sizeToFit()
    }
    
    func textLabelAttributes() -> [String : AnyObject]? {
        let attributes  = (textIsLight) ? TextSettings.textFontAttributesLight : TextSettings.textFontAttributesDark
        return attributes as [String : AnyObject]
    }
    
    func displayText() -> String {
        
        var theText = KeyStrings.syorz
        if textField.text?.characters.count > 0 {
            theText = textField.text!
        }
        
        return theText
    }
    //MARK: image picker
    
    func presentImagePicker() {
        
        if !canPresentImagePicker {
            return
        }
        
        canPresentImagePicker = false
        
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        
        picker.modalPresentationStyle = .fullScreen
        
        present(picker, animated: true, completion: nil)
    }
    
    func dismissImagePicker() {
        
        DispatchQueue.main.async(execute: {
            //self.dismissViewControllerAnimated(true, completion: nil)
            self.dismiss(animated: true, completion: { () -> Void in
                
                self.canPresentImagePicker = true
            })
        })
    }
    
    //MARK: keyboard
    
    func keyboardShown(_ notification: Notification) {
        
        originalTextConstraintConstant = textLabelBottomConstraint.constant
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        textLabelBottomConstraint.constant = (keyboardFrame.size.height + 8)
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardHidden(_ notification: Notification) {
        
        textLabelBottomConstraint.constant = originalTextConstraintConstant
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
}

extension CameraViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.capturedImage = chosenImage
            self.inCameraMode = false
            
        }
        
        dismissImagePicker()
    }
    //What to do if the image picker cancels.
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    
        dismissImagePicker()
    }
}

extension CameraViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
